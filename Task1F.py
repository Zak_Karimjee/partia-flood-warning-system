import floodsystem.stationdata as stationdata
import floodsystem.station as station

s = stationdata.build_station_list()
itrc = station.inconsistent_typical_range_stations(s)
itrc_names = sorted([s.name for s in itrc])
print(itrc_names)