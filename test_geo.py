# -*- coding: utf-8 -*-

import floodsystem.geo as geo
import floodsystem.stationdata as stationdata
import pytest
                       
def test_stations_by_distance():
    s = stationdata.build_station_list()
    my_coord = (52.206027, 0.115971)
    sbd = geo.stations_by_distance(s,my_coord)
    assert sbd[0][1]<sbd[1][1]

def test_stations_within_radius():
    camtenk = ['Bin Brook', 'Cambridge Baits Bite', "Cambridge Byron's Pool",
               'Cambridge Jesus Lock', 'Comberton', 'Dernford', 'Girton',
               'Haslingfield Burnt Mill', 'Lode', 'Oakington', 'Stapleford']
    cambridge = (52.2053, 0.1218)
    stations = stationdata.build_station_list()
    stations_rad = geo.stations_within_radius(stations,cambridge,10)
    stations_rad_names = [s.name for s,d in stations_rad]
    stations_rad_names = sorted(stations_rad_names)
    assert stations_rad_names == camtenk

def test_rivers_with_station():
    stations = stationdata.build_station_list();
    riversWithStation=geo.rivers_with_station(stations);
    assert len(riversWithStation) > 0;                                    
              
def test_stations_by_river():
    stations = stationdata.build_station_list();
    stationsByRiver=geo.stations_by_river(stations);
    assert len(stationsByRiver) > 0;
              
def test_rivers_by_station_number():
    stations = stationdata.build_station_list();
    stationNo = geo.rivers_by_station_number(stations,1);
    assert len(stationNo) >=1;                                         