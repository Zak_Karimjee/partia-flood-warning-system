# -*- coding: utf-8 -*-

import floodsystem.geo as geo
import floodsystem.stationdata as stationdata

cambridge = (52.2053, 0.1218)
stations = stationdata.build_station_list()
distance_stations = geo.stations_by_distance(stations,cambridge)
closest_stations = [(s.name,s.town,d) for s,d in distance_stations[:10]]
furthest_stations = [(s.name,s.town,d) for s,d in distance_stations[-10:]]
ttstations = closest_stations + furthest_stations
print(ttstations)