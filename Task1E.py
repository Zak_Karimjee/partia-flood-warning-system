# -*- coding: utf-8 -*-
"""
Created on Fri Feb  3 19:17:35 2017

@author: Matthew
"""

import floodsystem.stationdata as stationdata;
import floodsystem.geo as geo;


stations = stationdata.build_station_list();
                                         
riverStationNo=geo.rivers_by_station_number(stations,9);
print(riverStationNo)                                           
