"""Unit test for the station module"""

from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list

def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town
    trange = (10,-10)
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    assert s2.typical_range_consistent() == False
    trange = None
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    assert s3.typical_range_consistent() == False

def test_inconsistent_typical_range_stations():
    s = build_station_list()
    s_inconsistent = inconsistent_typical_range_stations(s)
    for sn in s_inconsistent:
        assert sn.typical_range_consistent() == False

