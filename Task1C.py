import floodsystem.geo as geo
import floodsystem.stationdata as stationdata

cambridge = (52.2053, 0.1218)
stations = stationdata.build_station_list()
stations_rad = geo.stations_within_radius(stations,cambridge,10)
stations_rad_names = [s.name for s,d in stations_rad]
stations_rad_names = sorted(stations_rad_names)

print(stations_rad_names)