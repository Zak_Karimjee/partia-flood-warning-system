import os
mods = os.listdir()
mods = [m for m in mods if os.path.isfile(m)]
mods = [m[:-3] for m in mods if m[0]!='_']

modules = map(__import__,mods)