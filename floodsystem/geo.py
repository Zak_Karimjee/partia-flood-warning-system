"""This module contains a collection of functions related to
geographical data.

"""

from math import radians, cos, sin, asin, sqrt
import operator as op

from .utils import sorted_by_key

def rivers_with_station(stations):
    riversWithStation=set([])
    for station in stations:
        riversWithStation.add(station.river)
    return riversWithStation  

def stations_by_river(stations):
    stationsByRiver = {}
    for station in stations:
        if station.river in stationsByRiver:
            stationsByRiver[station.river].append(station.name)
        else:
            stationsByRiver[station.river]=[station.name];
            
                   
    return stationsByRiver;  

def rivers_by_station_number(stations, N):
    #Fetch the number of stations on all rivers into dict, using river name as key
    riverStationNo = {}
    for station in stations:
        if station.river in riverStationNo:
            riverStationNo[station.river]+=1;
        else:
            riverStationNo[station.river]=1;
    
    riverStationNo = list(riverStationNo.items()); #convert dict to list
    #Sort them
    riverStationNo = sorted(riverStationNo, key=op.itemgetter(1)) ; #Ascending
    #Calculate amount to return
    if N < 0:
        N=0;
    
    while riverStationNo[-N-1][1]==riverStationNo[-N][1]:
        N+=1;

                   
    return riverStationNo[-N:];                 

def stations_by_distance(stations, p):
    '''
    returns the given list stations ordered by distance from the point p
    '''
    stationdist = []
    for station in stations:
        stationdist.append((station,_distance_between(p,station.coord)))
    stationdist = sorted_by_key(stationdist,1)
    return stationdist
    
def stations_within_radius(stations, centre, r):
    '''
    returns a list of stations within radius r in kilometres, of centre x
    '''
    #creates a distance sorted list
    s = stations_by_distance(stations,centre)
    i = 0
    for sd in s:
        if sd[1]>r:
            break
        i+=1
    #chops list at first index where distance is above r kilometres
    s = s[:i]
    return s
    
def _distance_between(a,b):
    '''
    returns the distance between point a & point b using the haversine formula
    underscore to indicate it's an internal function
    '''
    AVG_EARTH_RADIUS = 6371
    lata,longa = a
    latb,longb = b
    p1,p2,l1,l2 = map(radians,(lata,latb,longa,longb))
    # converts the latitudes and longitudes into radians for the haversine formula
    
    def haversine(x):
        '''
        Defines the haversine function to make the formula below easier
        '''
        return (sin(x/2))**2
    
    d = 2*AVG_EARTH_RADIUS*sqrt(haversine(p2-p1) + cos(p1)*cos(p2)*haversine(l2-l1))
    return d

    