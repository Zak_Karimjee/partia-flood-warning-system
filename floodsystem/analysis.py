# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 14:27:49 2017

@author: Matthew
"""

from .station import MonitoringStation
from .utils import sorted_by_key
import numpy as np
import matplotlib

def polyfit(dates, levels, p):
    
    x = matplotlib.dates.date2num(dates)
    x_shifted = x-x[0]
    p_coeff=np.polyfit(x_shifted, levels, p) #find coefficients for best fit polynomial of degree p
    
    poly=np.poly1d(p_coeff)
    
    return poly, x[0];
                    
    