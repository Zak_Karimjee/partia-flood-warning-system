# -*- coding: utf-8 -*-
"""
contains functions for plotting water levels
"""
import plotly
plotly.tools.set_credentials_file(username='zak.karimjee', api_key='Keu4cv8qGq64eVk4KRdc')
from plotly.graph_objs import *
import numpy as np
import datetime
from .analysis import polyfit
from .utils import abs2rel
from matplotlib.dates import date2num, num2date

def plot_water_levels(stations,dates,levels):
    """
    plots the relative water levels for the dates given for the stations given
    """
    if(hasattr(stations,'__iter__')):
        if(len(stations) != len(dates) or len(stations) != len(levels)):
            raise InputError("Must give same number of stations and lists of dates & water levels")
    else:
        #gets over the fact that this expects a list
        stations = [stations]
        dates = [dates]
        levels = [levels]
    data = []
    for s,d,l in zip(stations,dates,levels):
        #convert levels to relative levels
        rel_l = list(map(lambda x: ((x-s.typical_range[0])/(s.typical_range[1]-s.typical_range[0])), l))
        level_trace = Scatter(x=d,y=rel_l,name=s.name)
        data.append(level_trace)
    
    if(len(stations)>1):
        lyt = Layout(title = "Recent Water Levels", xaxis = dict(title="Date"), yaxis = dict(title="Relative Water Level (as fraction of typical range)"),
                     shapes = [{'type':'line', 'xref':'x', 'x0':dates[0][-1], 'x1':datetime.datetime.now(),'y0':1,'y1':1,
                                'line':{'dash':'dash','width':2,'color':'rgb(255,0,0)'}}])
    else:
        lyt = Layout(title = stations[0].name, xaxis = dict(title="Date"), yaxis = dict(title="Relative Water Level (as fraction of typical range)"),
                     shapes = [{'type':'line', 'xref':'x', 'x0':dates[0][-1], 'x1':datetime.datetime.now(),'y0':1,'y1':1,
                                'line':{'dash':'dash','width':2,'color':'rgb(255,0,0)'}}])
    graph = Figure(data=data, layout=lyt)
    plotly.offline.plot(graph, filename = "plots/water_levels" + str(datetime.datetime.today().strftime('%Y%m%d-%H%M%S')) + ".html")
    
    
def plot_water_level_with_fit(stations,dates,levels,p):
    """
    plots the relative water levels for the dates given for the stations given
    """
    if(hasattr(stations,'__iter__')):
        if(len(stations) != len(dates) or len(stations) != len(levels)):
            raise InputError("Must give same number of stations and lists of dates & water levels")
    else:
        #gets over the fact that this expects a list
        stations = [stations]
        dates = [dates]
        levels = [levels]
    data = []
    for s,d,l in zip(stations,dates,levels):
        #convert levels to relative levels
        rel_l = list(map(lambda x: ((abs2rel(x,s.typical_range))), l))
        #generate polynomial trace
        poly, date_shift = polyfit(d,l,p)
        poly_levels = []
        for date in d:
            rel_poly_level = (abs2rel(poly(date2num(date)-date2num(d[0])),s.typical_range))
            poly_levels.append(rel_poly_level)
        level_trace = Scatter(x=d,y=rel_l,name=s.name)
        poly_trace = Scatter(x=d,y=poly_levels,name="Polynomial fit: " + s.name)
        print(list(zip(rel_l,poly_levels)))
        data.append(level_trace)
        data.append(poly_trace)
    
    if(len(stations)>1):
        lyt = Layout(title = "Recent Water Levels", xaxis = dict(title="Date"), yaxis = dict(title="Relative Water Level (as fraction of typical range)"),
                     shapes = [{'type':'line', 'xref':'x', 'x0':dates[0][-1], 'x1':datetime.datetime.now(),'y0':1,'y1':1,
                                'line':{'dash':'dash','width':2,'color':'rgb(255,0,0)'}}])
    else:
        lyt = Layout(title = stations[0].name, xaxis = dict(title="Date"), yaxis = dict(title="Relative Water Level (as fraction of typical range)"),
                     shapes = [{'type':'line', 'xref':'x', 'x0':dates[0][-1], 'x1':datetime.datetime.now(),'y0':1,'y1':1,
                                'line':{'dash':'dash','width':2,'color':'rgb(255,0,0)'}}])
    graph = Figure(data=data, layout=lyt)
    plotly.offline.plot(graph, filename = "plots/water_levels" + str(datetime.datetime.today().strftime('%Y%m%d-%H%M%S')) + ".html")