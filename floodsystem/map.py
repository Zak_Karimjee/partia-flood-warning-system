# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 18:40:54 2017

@author: Zak Karimjee
"""

import plotly
plotly.tools.set_credentials_file(username='zak.karimjee', api_key='Keu4cv8qGq64eVk4KRdc')
from plotly.graph_objs import *
import datetime

def map_stations(stations, title='Relative Water Level Stations'):
    #scale for colours

    scl = [[0,"rgb(255,100,50)"]]
    colres = 10
    for i in range(colres):
        scl.append([i/colres,"rgb(" + str(i*255/colres) + ",100," + str(255-(i*255/colres)) + ")"])
    
    data = {'type':'scattergeo','lon':[],'lat':[],'text':[],'mode':'markers',
            'marker':{
                    'size':15,
                    'opacity':0.9,
                    'autocolorscale':False,
                    'symbol':'square',
                    'colorscale':scl,
                    'cmin':0,
                    'cmax':0.8,
                    'color':[]
                    
                    }}
    #build data for plotly
    for s in stations:
        rel_level = s.relative_water_level()
        data['lat'].append(s.coord[0])
        data['lon'].append(s.coord[1])
        data['text'].append(s.name + "," + str(rel_level))
        data['marker']['color'].append(rel_level)
        print(rel_level)
        if rel_level and rel_level>data['marker']['cmax']:
            data['marker']['cmax'] = rel_level
        #print(data['lon'][-1],data['lat'][-1],data['text'][-1])

    
    layout = {'title':title,
              'geo':{'scope':'europe',
                     'projection':{'type':'natural earth',
                                   'scale':3,
                                   },
                     'showland':True,
                     'landcolor':'rgb(250,250,250)',
                     'subunitcolor':'rgb(217,217,217)',
                     'countrycolor':'rgb(217,217,217)',
                     'countrywidth':0.5,
                     'subunitwidth':0.5,
                     'resolution':50,
                     'showcoastlines':True,
                     'showrivers':True,
                     'showsubunits':True,
                     'lataxis':{'range':[40.0,65.0]},
                     'lonaxis':{'range':[-20.0,15.0]}
                      },
                     
                     'mapbox':{'center':{'lon':-0.5,
                                  'lat':51.5},
                             'domain':{
                             'x':[-1,1], 'y':[-1,1]
                             },
                      'zoom':1
                      }}
    fig = {'layout':layout,'data':[data]}
    plotly.offline.plot(fig, filename = "plots/station_map" + str(datetime.datetime.today().strftime('%Y%m%d-%H%M%S')) + ".html")