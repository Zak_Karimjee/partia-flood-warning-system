"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""

class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        # stored as tuple (low,high)
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += " measure id: {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d
    
    def typical_range_consistent(self):
        if self.typical_range:
            if tuple(sorted(self.typical_range))==self.typical_range:
                return True
            else:
                return False
        else:
            return False
        
    def relative_water_level(self):
        """
        calculates the current water level as a relative number 
        where 0 is the bottom of the typical range and 1 is the top of the typical range
        """
        sanlevel = self.sanitise_latest_level()
        if self.typical_range_consistent():
            if sanlevel:
                rel_level = (sanlevel-self.typical_range[0])/(self.typical_range[1]-self.typical_range[0])
            else:
                return None
        else:
            return None
        return rel_level
    
    def sanitise_latest_level(self):
        """
        sometimes water levels aren't given as a single float (That'd be too obvious), sometimes as a list
        so it takes the maximum from the list if it's a list
        if it's not a list or convertable to a float then it returns none
        """
        if isinstance(self.latest_level,float):
            if hasattr(self.latest_level,"__iter__"):
                return float(max(self.latest_level))
            else:
                try:
                    return float(self.latest_level)
                except ValueError:
                    return None
                
def inconsistent_typical_range_stations(stations):
    itrc = [s for s in stations if not s.typical_range_consistent()]
    return itrc