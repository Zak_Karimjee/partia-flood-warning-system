# -*- coding: utf-8 -*-
"""
contains functions for plotting water levels
"""
import plotly
plotly.tools.set_credentials_file(username='zak.karimjee', api_key='Keu4cv8qGq64eVk4KRdc')
import plotly.plotly as plt
from plotly.graph_objs import *
import numpy as np

def plot_water_levels(stations,dates,levels):
    """
    plots the relative water levels for the dates given for the stations given
    """
    
    if(len(stations) != len(dates) or len(stations) != len(levels)):
        if(len(stations!=1)):
            raise InputError("Must give same number of stations and lists of dates & water levels")
    data = []
    for s,d,l in zip(stations,dates,levels):
        #convert levels to relative levels
        rel_l = list(map(lambda x: ((x-s.typical_range[0])/(s.typical_range[1]-s.typical_range[0])), l))
        level_trace = Scatter(x=d,y=rel_l,name=s.name)
        data.append(level_trace)
    #add lines for top & bottom of typical range
    max_trace = Scatter(x=np.full(len(dates[0]),1), y = dates[0], name = "Typical Maximum level")
    min_trace = Scatter(x=np.full(len(dates[0]),0), y = dates[0], name = "Typical Minimum level")
    
    
    lyt = Layout(title = "Recent Water Levels", xaxis = dict(title="Date"), yaxis = dict(title="Relative Water Level (as fraction of typical range)"))
    graph = Figure(data=data, layout=lyt)
    plotly.offline.plot(graph, filename = "plots/water_levels.html")