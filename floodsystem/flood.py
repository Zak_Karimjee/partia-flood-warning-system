"""
this contains functions related to flood monitoring
"""

from .station import MonitoringStation
from .utils import sorted_by_key,abs2rel
import floodsystem.analysis as analysis
from floodsystem.datafetcher import fetch_measure_levels
import datetime

def stations_level_over_threshold(stations,tol):
    """
    returns all stations from array stations where the current relative water level is over tol.
    Output of form of [(station,current_relative_water_level),...]
    """
    threshold_stations = []
    for s in stations:
        rel_level = s.relative_water_level()
        if rel_level:
            if rel_level>=tol:
                threshold_stations.append((s,rel_level))
    sorted_stations = sorted_by_key(threshold_stations,1,reverse=True)
    return sorted_stations

def stations_highest_rel_level(stations, N):
    """
    returns N stations with highest relative water level
    """
    #Fetch the relative levels of all stations into a dict, using stat name as key
    stationLevels=[]
    for station in stations:
        if station.relative_water_level():        
            stationLevels.append((station,station.relative_water_level()));
    #convert dict to list
 
    stationLevels = sorted_by_key(stationLevels, 1, True) ;#Descending
    ### the first few stations seem to give dud data so skip em
    return stationLevels[:N]      

def towns_at_risk(stations,N):
    """
    find the towns most at risk by considering N stations
    """
    #Start by finding stations at risk
    highestRelStations = stations_highest_rel_level(stations,N); #import N stations
    #Evaluate the risk to these stations
    
    townsAtRisk={}
    riversCountedByTown={}
    
    for station in highestRelStations:
        if station[0].name in riversCountedByTown:
            riversCountedByTown[station[0].town]+=1;
        else:
            riversCountedByTown[station[0].town]=1;
        #fetch last 5 days of data for this station
        dates, levels = fetch_measure_levels(station[0],dt=datetime.timedelta(days=3))
        relLevels=[];
        for index in range(len(levels)):
            relLevel = abs2rel(levels[index],station[0].typical_range)
            relLevels = relLevels + [relLevel, ] 
            #Now we have relative levels over the last 3 days for the iterate station
        
        #Calculate the average change in relative water level over the last 3 days
        relChangePerDay = (relLevels[-1] - relLevels[0])/5
        #Use weighted average to calculate risk
        if relChangePerDay >0:
            if relLevels[-1] < 1:
                #INCREASE TOWN COUNTER BY 1
                if station[0].name in townsAtRisk:
                    townsAtRisk[station[0].town]+=1
                else:       
                    townsAtRisk[station[0].town]=1
            else:

                #INCREASE TOWN COUNTER BY 2
                if station[0].name in townsAtRisk:
                    townsAtRisk[station[0].town]+=2
                else:
                    townsAtRisk[station[0].town]=2
    #NOW OUT OF FOR LOOP - ALL STATIONS ADDED TO RUNNING RISK TOTAL
    for N in townsAtRisk:
        townsAtRisk[N]=townsAtRisk[N]/riversCountedByTown[N]
        if townsAtRisk[N] >1.5:
            townsAtRisk[N]='Severe'
        elif townsAtRisk[N] <=1.5 and townsAtRisk[N] > 1.0:
            townsAtRisk[N]='High'
        elif townsAtRisk[N] <=1.0 and townsAtRisk[N] > 0.5:
            townsAtRisk[N]='Moderate'
        else:
            townsAtRisk[N]='Low'
    #Loop thru towns at risk, divide by number counted
    return townsAtRisk                       
        
    
    
                                           
              