# -*- coding: utf-8 -*-
"""unit test for the flood module"""

from floodsystem.stationdata import build_station_list, update_water_levels
import floodsystem.flood as flood

def test_stations_level_over_threshold():
    stations = build_station_list()
    update_water_levels(stations)
#    for s in stations:
#        print(s.name, s.latest_level)
    thresh_stations = flood.stations_level_over_threshold(stations,0.8)
    for s in thresh_stations:
        assert s[1]>0.8
                
def test_stations_highest_rel_level():
    stations = build_station_list()
    update_water_levels(stations)
    testReturn=flood.stations_highest_rel_level(stations,10)

    assert len(testReturn) == 10
              
    for count in range(10):
        assert testReturn[count][1] 
        assert testReturn[count][0]
        
def test_towns_at_risk():
    stations = build_station_list()
    update_water_levels(stations)
    risky_towns = flood.towns_at_risk(stations,10)
    for town,risk in risky_towns.items():
        assert risk in ['Severe','High','Moderate','Low']
    
    
test_towns_at_risk()