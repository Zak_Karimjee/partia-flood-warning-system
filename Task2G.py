# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 15:52:09 2017

@author: Matthew
"""

import datetime
import floodsystem.stationdata as stationdata;
import floodsystem.flood as flood;


# Build list of stations
stations = stationdata.build_station_list()
stationdata.update_water_levels(stations)

print(flood.towns_at_risk(stations, 15));