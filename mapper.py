# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 19:04:44 2017

@author: Zak Karimjee
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.map import map_stations
from floodsystem.flood import stations_highest_rel_level, stations_level_over_threshold

stations = build_station_list()
update_water_levels(stations)
high_stations,levels = zip(*stations_level_over_threshold(stations,0.8))
map_stations(high_stations, "Stations with relative water level over 0.8")