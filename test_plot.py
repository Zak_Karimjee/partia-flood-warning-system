# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 15:21:20 2017

@author: Zak Karimjee
"""

import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels,plot_water_level_with_fit
from floodsystem.flood import stations_highest_rel_level

def test_plot_water_levels():
    stations = build_station_list()
    update_water_levels(stations)
    # Find station 'Cam'
    high_stations,current_levels = zip(*stations_highest_rel_level(stations,5))
#    high_stations = high_stations[2:]
    # Fetch data over past 2 days
    dt = 2
    dates,levels = zip(*[fetch_measure_levels(s,dt=datetime.timedelta(days=dt)) for s in high_stations])
    plot_water_levels(high_stations,dates,levels)
    
def test_plot_water_level_with_poly():
    stations = build_station_list()
    update_water_levels(stations)
    # Find station 'Cam'
    high_stations,current_levels = zip(*stations_highest_rel_level(stations,5))
#    high_stations = high_stations[2:]
    # Fetch data over past 2 days
    dt = 2
    dates,levels = zip(*[fetch_measure_levels(s,dt=datetime.timedelta(days=dt)) for s in high_stations])
    plot_water_level_with_fit(high_stations,dates,levels,4)