# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 15:36:07 2017

@author: Zak Karimjee
"""

import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.flood import stations_highest_rel_level

def run():

    # Build list of stations
    stations = build_station_list()
    update_water_levels(stations)
    # Find station 'Cam'
    high_stations,current_levels = zip(*stations_highest_rel_level(stations,5))
#    high_stations = high_stations[2:]
    # Fetch data over past 2 days
    dt = 2
    dates,levels = zip(*[fetch_measure_levels(s,dt=datetime.timedelta(days=dt)) for s in high_stations])
    plot_water_level_with_fit(high_stations,dates,levels,4)

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")

    run()
