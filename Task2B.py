# -*- coding: utf-8 -*-
"""
Created on Mon Feb  6 15:15:15 2017

Demo program that prints all stations where the water level is over 0.8 of the typical range

@author: Zak Karimjee
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold

def run():
    stations = build_station_list()
    update_water_levels(stations)
#    for s in stations:
#        print(s.name, s.latest_level)
    thresh_stations = stations_level_over_threshold(stations,0.8)
    for s in thresh_stations:
        print(s[0].name,s[1])
        
if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")

    run()