# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 19:04:44 2017

@author: Zak Karimjee
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.map import map_stations

stations = build_station_list()
update_water_levels(stations)
map_stations(stations)