# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 14:37:04 2017

@author: Matthew
"""

import floodsystem.stationdata as stationdata;
import floodsystem.flood as flood;

stations = stationdata.build_station_list();
stationdata.update_water_levels(stations);
riverLevels=flood.stations_highest_rel_level(stations, 10);
print(riverLevels);      
print(len(riverLevels))                                     