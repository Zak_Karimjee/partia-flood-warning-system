# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 18:23:02 2017

@author: Matthew
"""

import floodsystem.stationdata as stationdata;
import floodsystem.geo as geo;

stations = stationdata.build_station_list();
riversWithStation=geo.rivers_with_station(stations);
print(len(riversWithStation));  
     
riverList=list(riversWithStation);
riverList.sort()     
     
for river in riverList[:10]:
    print(river)
                               
stationsByRiver=geo.stations_by_river(stations);  
                                     
print();                                     
                                     
print(sorted(stationsByRiver['River Aire']))                 
print(stationsByRiver['River Cam']);  
print(stationsByRiver['Thames']);  